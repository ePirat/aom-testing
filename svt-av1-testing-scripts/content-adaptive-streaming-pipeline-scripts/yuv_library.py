def getyuvlist():

    seq_list = []

    ##1080p-objective-1-fast:
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 29.97	, 'fps_denom' : 1001	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'aspen_1080p_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60	, 'fps_denom' : 1000	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'DOTA2_60f_420'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 50000 , 'fps' : 50 , 'fps_denom' : 1000	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'ducks_take_off_1080p50_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'life_1080p30_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'MINECRAFT_60f_420'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_Aerial_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_Boat_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_Crosswalk_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_FoodMarket_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_PierSeaside_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_SquareAndTimelapse_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_TunnelFlag_1920x1080_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 25000 , 'fps' : 25 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'rush_hour_1080p25_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'STARCRAFT_60f_420'}) ##FIXED
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 29.97 , 'fps_denom' : 1001 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'touchdown_pass_1080p_60f'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'wikipedia_420'})

    ##sub1080p-objective-1-fast:
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 25000 , 'fps' : 25 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'blue_sky_360p_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'dark720p_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 50000 , 'fps' : 50 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'gipsrestat720p_60f'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'kirland360p_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'KristenAndSara_1280x720_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_DrivingPOV_1280x720_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'Netflix_RollerCoaster_1280x720_60fps_8bit_420_60f'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'niklas360p_60f'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 29.97 , 'fps_denom' : 1001 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'red_kayak_360p_60f'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 50000 , 'fps' : 50 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'shields_640x360_60f'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 29.97 , 'fps_denom' : 1001 	, 'intra' : 119	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'speed_bag_640x360_60f'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 30	, 'fps_denom' : 1000 	, 'intra' : 119	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'thaloundeskmtg360p_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'vidyo1_720p_60fps_60f'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'vidyo4_720p_60fps_60f'})

    ##1080p-objective-1-fast:
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 29.97	, 'fps_denom' : 1001	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'aspen_1080p_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60	, 'fps_denom' : 1000	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'DOTA2_60f_420_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 50000 , 'fps' : 50 , 'fps_denom' : 1000	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'ducks_take_off_1080p50_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'life_1080p30_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'MINECRAFT_60f_420_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_Aerial_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_Boat_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_Crosswalk_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_FoodMarket_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_PierSeaside_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_SquareAndTimelapse_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_TunnelFlag_1920x1080_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 25000 , 'fps' : 25 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'rush_hour_1080p25_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'STARCRAFT_60f_420_10bit'}) 
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 29.97 , 'fps_denom' : 1001 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'touchdown_pass_1080p_60f_10bit'})
    seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'wikipedia_420_10bit'})

    ##sub1080p-objective-1-fast:
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 25000 , 'fps' : 25 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'blue_sky_360p_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'dark720p_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 50000 , 'fps' : 50 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'gipsrestat720p_60f_10bit'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'kirland360p_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'KristenAndSara_1280x720_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_DrivingPOV_1280x720_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'Netflix_RollerCoaster_1280x720_60fps_8bit_420_60f_10bit'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 30 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'niklas360p_60f_10bit'}) ##Fixed
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 29.97 , 'fps_denom' : 1001 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'red_kayak_360p_60f_10bit'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 50000 , 'fps' : 50 , 'fps_denom' : 1000 	, 'intra' : 119 	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'shields_640x360_60f_10bit'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 29.97 , 'fps_denom' : 1001 	, 'intra' : 119	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'speed_bag_640x360_60f_10bit'})
    seq_list.append({'tbr' : 1000 , 'qp' : 32 , 'width' : 640 	, 'height' : 360, 'fps_num' : 30000 , 'fps' : 30	, 'fps_denom' : 1000 	, 'intra' : 119	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'thaloundeskmtg360p_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119	, 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'vidyo1_720p_60fps_60f_10bit'})
    seq_list.append({'tbr' : 2500 , 'qp' : 32 , 'width' : 1280 	, 'height' : 720, 'fps_num' : 60000 , 'fps' : 60 , 'fps_denom' : 1000 	, 'intra' : 119 , 'bitdepth' : 10   , 'unpacked' : 0  , 'name':'vidyo4_720p_60fps_60f_10bit'})


    return seq_list
