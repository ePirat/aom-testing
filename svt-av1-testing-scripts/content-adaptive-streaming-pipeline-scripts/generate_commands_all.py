# Copyright (c) 2019, Alliance for Open Media. All rights reserved
#
# This source code is subject to the terms of the BSD 2 Clause License and
# the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
# was not distributed with this source code in the LICENSE file, you can
# obtain it at www.aomedia.org/license/software. If the Alliance for Open
# Media Patent License 1.0 was not distributed with this source code in the
# PATENTS file, you can obtain it at www.aomedia.org/license/patent.

import os
import stat
import re
from copy import deepcopy

###################################### =====Configuration Start ===== ######################################
#Location of the clips to be encoded
CLIP_DIRS = [r"content_adaptive_streaming_clips"]

#ENC_MODES               = [1]
ENC_MODES                = [7,8]
#1-pass or 2-pass encoding, not supported by all encoders
passes                   = 1
#enable/disable CQP encodings
cqp                      = 0

#Enable/disable cutting clips based on text file
scene_cut_clip                 = 0
#Suffix of files containing a new line separated list of frame boundaries
#Place in same folder as clips
#i.e: clip1.y4m, clip1_scenes.txt
scene_changes_suffix     = "_scenes.txt"

#Enable/Disable Downscaling clips with downscaling filter specified below
downscale_commands       = 1
#Downscaling algorithm to use. please see https://ffmpeg.org/ffmpeg-scaler.html for
#all options
downscale_algo           = "lanczos"
#downscale_algo           = "bilinear"
#downscale_algo           = "bicubic"
#downscale_algo           = "gauss"
#downscale_algo           = "sinc"

#Generates PSNR/SSIM/VMAF commands for rescale pipeline
rescale_bdrate           = 1
#target_resolutions             = [("width1", "height1"),("width2", "height2")]
#target_resolutions             = [(1280,720),(960,540), (768,432), (608, 342), (480, 270), (384, 216)] #10bit
target_resolutions             = [(1280,720),(960,540),(640,360),(480, 270)] #8bit

##### If rescale_bdrate is on the following options should be 0 #####
#Enable/disable ffmpeg command generation for non-rescale pipeline
psnr_ssim_generation        = 0
#Enable/disable vmaf command generation for non-rescale pipeline
vmaf_generation             = 0
#####################################################################

#Enable/disable usage of vmaf disable enhancement model
disable_enhancement_vmaf    = 0

#Delete intermediary YUV/Y4M files after collecting data
delete_intermediary         = 0



#Number of simultaneous jobs to use for encoding and ffmpeg respectively
parallel_job_count       = 48
ffmpeg_job_count         = 20

bitstream_folders = ['bitstreams/']

ENCODER                 = "svt"
##ENCODER                 = "libaom"
##ENCODER                 = "vtm"
##ENCODER                 = "hm"
##ENCODER                  = "x264"
##ENCODER                  = "x265"
##ENCODER                  = "vp9"
##ENCODER                  = "ffmpeg"


if ENCODER == "vtm" or ENCODER == "hm" or ENCODER == 'x264' or ENCODER == 'x265':
    QP_VALUES               = [22,27,32,37,47]
    #QP_VALUES              = [14, 18, 22, 27 ,32, 37, 42, 47, 51]
else:
    QP_VALUES               = [20,32,43,55,63]
    # QP_VALUES               = [20,26,32,37,43,48,55,59,63]

encoder_cfg_list = []
if ENCODER == "libaom" or ENCODER == "vp9":
    encoder_cfg_list.append({'enc_name': 'aom','config_token': ' -c ','enc_mode_token': '--cpu-used=','fps_token': '--fps=',
                             'input_token': '  ','width_token': '--width=','height_token': '--height=','qp_value_token': '--cq-level=',
                             'fps_num': '-fps-num','fps_denom': '-fps-denom','intra_period_token': '--kf-min-dist=',
                             'intra_period_token_max': '--kf-max-dist=','bitdepth_token': '--bit-depth=','inbitdepth_token': '--input-bit-depth=',
                             'bitstream_token': ' -o ','num_frames_token': ' -n ','rc_token': ' -rc ','tbr_token': ' -tbr ','mbr_token': ' -mbr ',
                             'mbs_token': ' -mbs ','out_1pass': ' -use-output-stat-file ','in_2pass': ' -use-input-stat-file ',
                             'lad_token' : '-lad', 'lp_token': '-lp', 'passes_token': ' --passes=', 'qp_mode_token': '--end-usage=q ',
                             'auto_alt_ref_token': '--auto-alt-ref=', 'lag_in_frames_token': '--lag-in-frames=',
                             'use_fixed_qp_offsets_token': '--use-fixed-qp-offsets=', 'enable_tpl_model_token': '--enable-tpl-model=',
                             'min_gf_interval_token': '--min-gf-interval=', 'max_gf_interval_token': '--max-gf-interval=',
                             'gf_min_pyr_height_token': '--gf-min-pyr-height=', 'gf_max_pyr_height_token': '--gf-max-pyr-height=',
                             'aq_mode_token': '--aq-mode=', 'deltaq_token': '--deltaq-mode=', 'threads_token': ' --threads='})

elif ENCODER == "svt":
    encoder_cfg_list.append({'enc_name': 'svt-av1','config_token': ' -c ','enc_mode_token': '-enc-mode','input_token': ' -i ',
                             'width_token': '-w','height_token': '-h','qp_value_token': '-q','fps_num': '-fps-num',
                             'fps_denom': '-fps-denom','intra_period_token': ' -intra-period ','bitdepth_token': '-bit-depth',
                             'bitstream_token': ' -b ','num_frames_token': ' -n ','rc_token': ' -rc ','tbr_token': ' -tbr ',
                             'mbr_token': ' -mbr ','mbs_token': ' -mbs ','out_1pass': ' -use-output-stat-file ',
                             'in_2pass': ' -use-input-stat-file ', 'lad_token' : '-lad', 'lp_token': '-lp',
                             'passes_token': '--passes ', 'irefresh_type_token': '--irefresh-type ',
                             'enable_tpl_model_token':'--enable-tpl-la '})
elif ENCODER =="x264" or ENCODER == "x265":
    encoder_cfg_list.append({'enc_name': ENCODER, 'enc_mode_token': ' --preset ', 'input_res_token': ' --input-res ',
                            'threads_token': ' --threads ', 'tune_token': ' --tune ', 'stats_token': ' --stats ',
                            'qp_value_token': ' --crf ', 'fps_token': ' --fps ', 'bitdepth_token': ' --input-depth ',
                            'intra_period_token': ' --keyint ', 'intra_period_token_min': ' --min-keyint ', 'no_scenecut_token': ' --no-scenecut ',
                            'bitstream_token' : ' -o ', 'frame_threads_token': ' --frame-threads', 'no_wpp_token': ' --no-wpp '})

elif ENCODER =="ffmpeg":
    encoder_cfg_list.append({'enc_name': 'ffmpeg', 'enc_mode_token': ' -preset ', 'input_res_token': ' -s:v ',
                            'threads_token': ' --threads ', 'qp_value_token': ' -qp ', 'fps_token': ' -r ', 'bitdepth_token': ' -pix_fmt ', 'input_token': ' -i ', 'intra_period_token': " -g ", 'lad_token': '-la_depth '})
else :
    encoder_cfg_list.append({'enc_name': 'svt-av1','config_token': ' -c ','enc_mode_token': '-enc-mode','input_token': ' -i ',
                             'width_token': '-wdt','height_token': '-hgt','qp_value_token': '-q','fps':'-fr','fps_num': '-fps-num',
                             'fps_denom': '-fps-denom','intra_period_token': ' -ip ','bitdepth_token': '-bit-depth',
                             'bitstream_token': ' -b ','num_frames_token': ' -f ','rc_token': ' -rc ','tbr_token': ' -tbr ',
                             'mbr_token': ' -mbr ','mbs_token': ' -mbs ','out_1pass': ' -use-output-stat-file ',
                             'in_2pass': ' -use-input-stat-file ', 'lad_token' : '-lad', 'lp_token': '-lp',
                             'bitdepth_token': '--bit-depth=','inbitdepth_token': '--input-bit-depth='})

####################################### =====Configuration End ===== #######################################

if passes != 1 and passes != 2:
    print("WARNING: "+"passes is "+str(passes)+". passes should be 1 or 2")
if psnr_ssim_generation != 0 and psnr_ssim_generation != 1:
    print("WARNING: "+"psnr_ssim_generation is "+str(psnr_ssim_generation)+". psnr_ssim_generation should be 0 or 1.")
    quit()
if scene_cut_clip not in [0,1]:
    print("WARNING: "+"scene_cut_clip is "+str(scene_cut_clip)+". scene_cut_clip should be 0 or 1.")
    quit()
if rescale_bdrate not in [0,1]:
    print("WARNING: "+"rescale_bdrate is "+str(rescale_bdrate)+". rescale_bdrate should be 0 or 1.")
    quit()
if downscale_commands not in [0,1]:
    print("WARNING: "+"downscale_commands is "+str(downscale_commands)+". downscale_commands should be 0 or 1.")
    quit()
if passes == 2 and (ENCODER == "x264" or ENCODER == "x265"):
    print("WARNING: 2 pass mode with x264 and x265 currently not supproted")
    quit()

clip_location = CLIP_DIRS[0] + "/"

yuv_library_found = 0
try:
    from yuv_library import *
    seq_list = getyuvlist()
    yuv_library_found = 1
except ImportError:
    print("WARNING yuv_library not found, only generating commands for y4m files.")
    seq_list = []
commands = []
if "collect_raw_bdr_speed_data.py" not in os.listdir("."):
    print("WARNING collect_raw_bdr_speed_data.py script missing")



def generate_bash_driver_file(parallel_job_count, ffmpeg_job_count, encoder, run_all_paral_file_name, run_paral_cpu_file_name, enc_modes,cqp,scene_cut_clip, bitstream_folders, delete_intermediary):

    if cqp == 1:
        encoder_test_identifier = encoder +"-" +str(passes)+"p-cqp"
    else:
        encoder_test_identifier = encoder +"-" +str(passes)+"p"

    encoder_exec = ""
    if encoder == "svt":
        encoder_exec = "SvtAv1EncApp"
    elif encoder == "libaom":
        encoder_exec = "aomenc"
    elif encoder == "x264":
        encoder_exec = "x264"
    elif encoder == "x265":
        encoder_exec = "x265"
    elif encoder == "vp9":
        encoder_exec = "vpxenc"
    elif encoder == "ffmpeg":
        encoder_exec = "ffmpeg"
    elif encoder == "vtm":
        encoder_exec = "EncoderAppStatic"
    elif encoder == "hm":
        encoder_exec = "TAppEncoder"
    else:
        print("Warning encoder executable not specified in bash script")

    with open(run_paral_cpu_file_name, 'w') as file:
        file.write("parallel -j {} < run-{}-m$1.txt".format(parallel_job_count, encoder_test_identifier))

    run_all_paral_script = []
    run_all_paral_script.append("#!/bin/bash")
    run_all_paral_script.append("encoder_type=\"{}\"".format(encoder))
    run_all_paral_script.append("enc_mode_array=({})".format(' '.join(map(str,enc_modes))))
    run_all_paral_script.append("debug_date=" + r"`date " + "\"+%Y-%m-%d_%H%M%S\"`")
    run_all_paral_script.append("debug_filename=" + "\"debug_output_automation_${debug_date}.txt\"")
    for folder in bitstream_folders:
        run_all_paral_script.append("mkdir {}".format(folder))
    run_all_paral_script.append("chmod +rx tools")
    run_all_paral_script.append("chmod +rx tools/*")
    run_all_paral_script.append("chmod +rx *.sh")
    run_all_paral_script.append("chmod +x {}".format(encoder_exec))
    if scene_cut_clip == 1 or downscale_commands == 1:
        run_all_paral_script.append("mkdir resized_clips")

    if scene_cut_clip ==1:
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j {} < run_scene_cut.txt) &> time_scene_cut.log &".format(ffmpeg_job_count))
        run_all_paral_script.append("wait && echo \'Scene Cutting Finished\'")
    if downscale_commands == 1:
        run_all_paral_script.append("parallel -j {} < run_copy_reference.txt".format(ffmpeg_job_count))
        run_all_paral_script.append("(/usr/bin/time --verbose parallel -j 20 < run_downscale.txt) &> time_downscale.log &")
        run_all_paral_script.append("wait && echo \'Downscaling Finished\'")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    run_all_paral_script.append("\tif [ \"$encoder_type\" == \"libaom\" ] || [ \"$encoder_type\" == \"vp9\" ]; then")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) > time_enc_$i.log 2>&1 &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\telse")
    run_all_paral_script.append("\t\t(/usr/bin/time --verbose ./{} $i) &> time_enc_$i.log &".format(run_paral_cpu_file_name))
    run_all_paral_script.append("\tfi\n")
    run_all_paral_script.append("\twait $b && echo \'Encoding Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    run_all_paral_script.append("for i in \"${enc_mode_array[@]}\"; do")
    run_all_paral_script.append("\techo \'Running M\'$i")
    if psnr_ssim_generation ==1 :
        run_all_paral_script.append("\t(parallel -j {} < run-ffmpeg-{}-m$i.txt) &> time_ffmpeg_$i.log &".format(ffmpeg_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'PSNR/SSIM Generation for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    if vmaf_generation ==1 :
        run_all_paral_script.append("\t(parallel -j {} < run-vmaf-{}-m$i.txt) &> time_vmaf_$i.log &".format(ffmpeg_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'VMAF Generation for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    if rescale_bdrate == 1 :
        run_all_paral_script.append("\t(/usr/bin/time --verbose parallel -j {} < run-rescale-metrics-{}-m$i.txt) &> time_rescale_metrics_$i.log &".format(ffmpeg_job_count, encoder_test_identifier))
        run_all_paral_script.append("\twait $b && echo \'Rescaling VMAF/PSNR/SSIM Generation for Encode Mode \'$i\' Finished\' >> ${debug_filename}")
    run_all_paral_script.append("done")
    if delete_intermediary == 1:
        run_all_paral_script.append("rm resized_clips/*.yuv")
        run_all_paral_script.append("rm resized_clips/*.y4m")
        run_all_paral_script.append("rm bitstreams/*.yuv")
        run_all_paral_script.append("rm bitstreams/*.y4m")

    run_all_paral_script.append("python collect_raw_bdr_speed_data.py")


    with open(run_all_paral_file_name, 'w') as file:
        for line in run_all_paral_script:
            file.write(line + '\n')

    ## chmodding bash files
    # NOTE: os.chmod is finicky -> may not work
    file_stat = os.stat(run_all_paral_file_name)
    os.chmod(run_all_paral_file_name, file_stat.st_mode | stat.S_IEXEC)

    file_stat = os.stat(run_paral_cpu_file_name)
    os.chmod(run_paral_cpu_file_name, file_stat.st_mode | stat.S_IEXEC)

def read_y4m_header(clip_path):
    header_delimiters = ["W","H","F","I","A","C"]
    width = 0
    height = 0
    frame_ratio = ""
    framerate = 0

    with open(clip_path, "r") as f:
        f.seek(10)
        buffer = ""
        while True:
            readByte = f.read(1)
            if (readByte in header_delimiters):
                if readByte == header_delimiters[1]:
                    width = int(buffer)
                    buffer = ""
                elif readByte == header_delimiters[2]:
                    height = int(buffer)
                    buffer = ""
                elif readByte == header_delimiters[3]:
                    frame_ratio = buffer
                    buffer = ""
                    break #Break after reading the frame ratio
            else:
                buffer+=readByte

        frame_ratio_pieces = frame_ratio.split(":")
        framerate = float(frame_ratio_pieces[0])/float(frame_ratio_pieces[1])
    return width, height, framerate

def read_y4m_frame_count(clip_path, width, height):
    frame_count = 0
    frame_length_10 = width * height * 3
    frame_length_8 = (width * height * 3)/2
    frame_length = 0
    with open(clip_path, "r") as f:
        f.seek(10)
        i = 0
        color_space_found = 0
        while True:
            readByte = f.read(1)
            if not readByte:
                # EOF
                break
            if readByte == "C" and color_space_found == 0:
                colour_space = ""
                while not (readByte == str(chr(0x20)) or readByte == str(chr(0x0A))):
                    readByte = f.read(1)
                    colour_space+=readByte

                if colour_space == "420mpeg2 ":
                    frame_length = frame_length_8
                elif colour_space == "420p10 ":
                    frame_length = frame_length_10
                color_space_found = 1
            if readByte == "F":
                next4 = f.read(4)
                if next4 == "RAME":
                    frame_count+=1
                    f.seek(frame_length+1,1)
    return frame_count

def get_fps(clip_dir, clip):
    if(".yuv" in clip and yuv_library_found):
        seq_table_index = get_seq_table_loc(seq_list, clip)
        if seq_table_index < 0:
            return 0
        fps = float(seq_list[seq_table_index]["fps_num"]) / seq_list[seq_table_index]["fps_denom"]
        return fps
    elif(".y4m" in clip):
        _, _, framerate = read_y4m_header(os.path.join(clip_dir, clip))
        return framerate
    else:
        return 0

def get_clip_list(clip_dir):
    files = os.listdir(clip_dir)
    files.sort(key=lambda f: get_fps(clip_dir, f), reverse=True)
    files.sort(key=lambda f: f.lower())
    files.sort(key=lambda f: (os.stat(os.path.join(clip_dir, f)).st_size), reverse=True)
    Y4M_HEADER_SIZE = 80

    ## sort all files in lists by size
    clip_lists = [[]]
    clip_list = []
    size_0 = os.stat(os.path.join(clip_dir, files[0])).st_size
    for file_ in files:
        if (".yuv" in file_ or ".y4m" in file_):
            if ".yuv" in file_ and os.stat(os.path.join(clip_dir, file_)).st_size == size_0:
                clip_list.append(file_)
            elif ".y4m" in file_ and size_0 - Y4M_HEADER_SIZE<= os.stat(os.path.join(clip_dir, file_)).st_size<= size_0+Y4M_HEADER_SIZE:
                clip_list.append(file_)
            else:
                clip_lists.append(clip_list)
                clip_list = []
                size_0 = os.stat(os.path.join(clip_dir, file_)).st_size
                clip_list.append(file_)

    clip_lists.append(clip_list)
    return clip_lists

def get_seq_table_loc(seq_table, clipname):

    for i in range(len(seq_table)):
        if seq_table[i]["name"] == clipname[:-4]:
            return i


    return -1


def generate_commands(CLIP_DIRS, clip_location, ENC_MODES, passes, cqp, psnr_ssim_generation, vmaf_generation, disable_enhancement_vmaf,
                      scene_cut_clip, scene_changes_suffix, downscale_commands, rescale_bdrate, target_resolutions,
                      bitstreams, ENCODER, seq_list):

    ffmpeg_file = []
    vmaf_file = []
    fo = []
    rescale_metrics_file = []

    scene_cut_clips = [[]]
    scene_cut_map = {}

    if scene_cut_clip:
        with open("run_scene_cut.txt", "w") as g:
            for clip_directory in CLIP_DIRS:
                clip_lists = get_clip_list(clip_directory)
                for i in range(len(clip_lists)):
                    for clip_ in clip_lists[i]:
                        seq_table_index = get_seq_table_loc(seq_list, clip_)
                        if seq_table_index < 0 and ".yuv" in clip_:
                            continue
                        elif ".y4m" not in clip_ and ".yuv" not in clip_:
                            continue
                        total_number_of_frames = -1
                        if ".yuv" in clip_:
                            seq_list_entry = seq_list[seq_table_index]
                            width = seq_list_entry["width"]
                            height = seq_list_entry["height"]
                            bit_depth = seq_list_entry["bitdepth"]
                            ten_bit_format = seq_list_entry["unpacked"]

                            if bit_depth == 10:
                                pix_fmt = "yuv420p10le"
                                if ten_bit_format == 2:
                                    total_number_of_frames = (int)(((float)(os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/1.25)
                                else:
                                    total_number_of_frames = (int)(((os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/2)
                            elif bit_depth == 8:
                                pix_fmt = "yuv420p"
                                total_number_of_frames = (int)(os.path.getsize(os.path.join(clip_directory,clip_))/(width*height+(width*height/2)))
                        else:
                            width, height, _ = read_y4m_header(os.path.join(clip_directory,clip_))
                            total_number_of_frames = read_y4m_frame_count(os.path.join(clip_directory,clip_), width, height)

                        try:
                            with open(clip_directory+"/"+clip_[:-4]+scene_changes_suffix, "r") as f:
                                scene_cuts = f.readlines()
                                for i in range(len(scene_cuts)):
                                    try:
                                        frame_start = int(scene_cuts[i])
                                    except:
                                        print("{} could not be converted into an integer. Exiting".format(scene_cuts[i]))
                                        quit()
                                    if frame_start > total_number_of_frames:
                                        print("ERROR: cutting scene at frame {}, beyond length of the clip {}".format(frame_start, total_number_of_frames))
                                        quit()
                                    if i < len(scene_cuts)-1:
                                        try:
                                            frame_end = int(scene_cuts[i+1])-1
                                        except:
                                            print("{} could not be converted into an integer. Exiting".format(scene_cuts[i+1]))
                                            quit()
                                    else:
                                        frame_end = total_number_of_frames

                                    new_clip_name = clip_[:-4] + "_scene_{}".format(i) + clip_[-4:]

                                    target_path = "resized_clips/" + new_clip_name
                                    ffmpeg_string = "tools/ffmpeg"

                                    if ".yuv" in clip_:
                                        new_entry = deepcopy(seq_list_entry)
                                        new_entry["name"] = new_clip_name[:-4]
                                        new_entry["number_of_frames"] = frame_end - frame_start + 1

                                        seq_list.append(new_entry)

                                        ffmpeg_params = " -y -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 -i {} -vf select=\'between(n\\,{}\\,{})\' -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 {}".format(width, height, pix_fmt, os.path.join(clip_location, clip_), int(frame_start)-1, int(frame_end)-1, width, height, pix_fmt, target_path)
                                        if i == len(scene_cuts)-1:
                                            ffmpeg_params = " -y -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 -i {} -vf select=\'gte(n\\,{})\' -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 {}".format(width, height, pix_fmt, os.path.join(clip_location, clip_), int(frame_start)-1, width, height, pix_fmt, target_path)

                                    else:
                                        ffmpeg_params = " -y -i {} -vf select=\'between(n\\,{}\\,{})\' {}".format(os.path.join(clip_location, clip_), int(frame_start)-1, int(frame_end)-1, target_path)
                                        if i == len(scene_cuts)-1:
                                            ffmpeg_params = " -y -i {} -vf select=\'gte(n\\,{})\' {}".format(os.path.join(clip_location, clip_), int(frame_start)-1, target_path)

                                    scene_cut_map[new_clip_name] = {"width": width, "height": height}
                                    scene_cut_clips[0].append(new_clip_name)
                                    cutting_command = ffmpeg_string+ ffmpeg_params
                                    g.write(cutting_command+"\n")
                        except:
                            print("Error processing scenes file for the clip {}. Exiting".format(clip_))
                            quit()

    downscaled_clip_list = []
    downscaled_clip_map = {}

    if downscale_commands:
        work_env = os.getcwd()

        for target_res in target_resolutions:
            downscaled_clip_map["{}x{}".format(target_res[0], target_res[1])] = []
        with open("run_downscale.txt", "w") as f, open("run_copy_reference.txt", "w") as g:

            if ENCODER == "svt-av1": # Implemented for abbreviation purposes
                encode_file_name = "svt"
            else:
                encode_file_name = str(ENCODER)

            if scene_cut_clip == 1:
                resize_dir = "resized_clips/"
                CLIP_DIRS = [resize_dir]
                clip_location = resize_dir

            for clip_directory in CLIP_DIRS:
                if scene_cut_clip:
                    clip_lists = scene_cut_clips
                else:
                    clip_lists = get_clip_list(clip_directory)

                for i in range(len(clip_lists)):
                    for clip_ in clip_lists[i]:
                        seq_table_index = get_seq_table_loc(seq_list, clip_)
                        if seq_table_index < 0 and ".yuv" in clip_:
                            continue
                        elif ".y4m" not in clip_ and ".yuv" not in clip_:
                            continue

                        if "yuv" in clip_:
                            width = seq_list[seq_table_index]['width']
                            height = seq_list[seq_table_index]['height']
                            ten_bit_format = seq_list[seq_table_index]['unpacked']
                            bit_depth = seq_list[seq_table_index]['bitdepth']


                            if bit_depth == 10:
                                pix_fmt = "yuv420p10le"
                                if scene_cut_clip == 1:
                                    number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                else:
                                    if ten_bit_format == 2:
                                        number_of_frames = (int)(((float)(os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/1.25)
                                    else:
                                        number_of_frames = (int)(((os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/2)

                            elif bit_depth == 8:
                                pix_fmt = "yuv420p"
                                if scene_cut_clip == 1:
                                    number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                else:
                                    number_of_frames = (int)(os.path.getsize(os.path.join(clip_directory,clip_))/(width*height+(width*height/2)))

                            seq_list_entry = seq_list[seq_table_index]

                        if scene_cut_clip == 1:
                            width, height = scene_cut_map[clip_]["width"],scene_cut_map[clip_]["height"]
                        elif "y4m" in clip_:
                            width, height, _ = read_y4m_header(os.path.join(clip_directory,clip_))

                        match = re.search(r"_\d+x\d+_",clip_)
                        if match is None:
                            match = re.search(r"_\d+p.*?_",clip_)
                            if match is None:
                                match_start = len(clip_)-4
                                match_end = len(clip_)-4
                            else:
                                match_start = match.start()
                                match_end = match.end()
                        else:
                            match_start = match.start()
                            match_end = match.end()
                        ref_clip = clip_[:match_start] + "_{}x{}_".format(width, height) + clip_[match_end:]
                        if "{}x{}".format(width,height) not in downscaled_clip_map:
                            downscaled_clip_map["{}x{}".format(width, height)] = []

                        downscaled_clip_map["{}x{}".format(width, height)].append(ref_clip)

                        if not scene_cut_clip:
                            cp_command = "cp {} {}".format(os.path.join(clip_location, clip_), "resized_clips/"+ ref_clip +"\n")
                            g.write(cp_command)

                        if "yuv" in clip_:
                            seq_list[seq_table_index]["name"] = ref_clip[:-4]
                            seq_list[seq_table_index]["number_of_frames"] = number_of_frames

                        for target_res in target_resolutions:
                            target_width = target_res[0]
                            target_height = target_res[1]
                            if target_width >= width and target_height >= height:
                                continue
                            new_clip_name = clip_[:match_start] + "_{}x{}to{}x{}_{}_".format(width, height, target_width, target_height,downscale_algo[:4]) + clip_[match_end:]
                            downscaled_clip_map["{}x{}".format(target_width, target_height)].append(new_clip_name)

                            target_path = "resized_clips/" + new_clip_name

                            ffmpeg_string = "tools/ffmpeg"

                            if ".yuv" in clip_:
                                seq_list_entry = seq_list[seq_table_index]
                                new_entry = deepcopy(seq_list_entry)
                                new_entry["name"] = new_clip_name[:-4]
                                new_entry["width"] = target_width
                                new_entry["height"] = target_height
                                new_entry["number_of_frames"] = number_of_frames

                                seq_list.append(new_entry)

                                ffmpeg_params = " -y -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 -i {} -sws_flags {}+accurate_rnd+print_info -strict -1 -f rawvideo -s:v {}x{} -pix_fmt {} -r 30000/1001 {}".format(width, height, pix_fmt, os.path.join(clip_location, clip_), downscale_algo, target_width, target_height, pix_fmt, target_path)
                            else:
                                ffmpeg_params = " -y -i {} -sws_flags {}+accurate_rnd+print_info -strict -1 -s:v {}x{} {}".format(os.path.join(clip_location, clip_), downscale_algo, target_width, target_height, target_path)

                            downscale_command = ffmpeg_string+ ffmpeg_params
                            f.write(downscale_command+"\n")

        key_list = downscaled_clip_map.keys()
        key_list.sort(key=lambda k: int(k.split("x")[0]) * int(k.split("x")[1]), reverse=True)
        downscaled_clip_list.append(downscaled_clip_map[key_list[0]])
        for target_res in target_resolutions:
            downscaled_clip_list.append(downscaled_clip_map["{}x{}".format(target_res[0], target_res[1])])


    for enc_mode in ENC_MODES:
        # create all the txt files
        if cqp == 1:
            COMMANDS_FILE_NAME = "run-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
        else:
            COMMANDS_FILE_NAME = "run-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"

        fo.append(open(COMMANDS_FILE_NAME, "w"))
        if psnr_ssim_generation == 1 and rescale_bdrate != 1:
                if cqp == 1:
                    FFMPEG_FILE_NAME = "run-ffmpeg-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
                else:
                    FFMPEG_FILE_NAME = "run-ffmpeg-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
                ffmpeg_file.append(open(FFMPEG_FILE_NAME, "w"))

        if vmaf_generation == 1:
            if cqp == 1:
                VMAF_FILE_NAME = "run-vmaf-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
            else:
                VMAF_FILE_NAME = "run-vmaf-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
            vmaf_file.append(open(VMAF_FILE_NAME, "w"))

        if rescale_bdrate == 1:
            psnr_ssim_generation = 0
            vmaf_generation   = 0

            if cqp == 1:
                RESCALE_METRICS = "run-rescale-metrics-" + ENCODER +"-" +str(passes)+"p-cqp"+ "-m" + str(enc_mode) + ".txt"
            else:
                RESCALE_METRICS = "run-rescale-metrics-" + ENCODER +"-" +str(passes)+"p"+ "-m" + str(enc_mode) + ".txt"
            rescale_metrics_file.append(open(RESCALE_METRICS, "w"))

        for bitstream_folder in bitstream_folders:

            if downscale_commands or scene_cut_clip:
                resize_dir = "resized_clips/"
                CLIP_DIRS = [resize_dir]
                clip_location = resize_dir

            for clip_directory in CLIP_DIRS:

                if downscale_commands:
                    clip_lists = downscaled_clip_list
                elif scene_cut_clip:
                    clip_lists = scene_cut_clips
                else:
                    clip_lists = get_clip_list(clip_directory)

                for i in range(len(clip_lists)):
                    for qp_value in QP_VALUES:
                        for clip_ in clip_lists[i]:
                            seq_table_index = get_seq_table_loc(seq_list, clip_)
                            if seq_table_index < 0 and ".yuv" in clip_:
                                continue
                            elif ".y4m" not in clip_ and ".yuv" not in clip_:
                                continue
                            if yuv_library_found and ".yuv" in clip_:
                                bit_depth = seq_list[seq_table_index]['bitdepth']
                                ten_bit_format = seq_list[seq_table_index]['unpacked']
                                width = seq_list[seq_table_index]['width']
                                height = seq_list[seq_table_index]['height']

                                if (bit_depth == 8):
                                    if ENCODER=="vtm":
                                        config_used = "encoder_randomaccess_vtm.cfg"
                                    else :
                                        config_used ="encoder_randomaccess_main.cfg"
                                    if downscale_commands or scene_cut_clip:
                                        number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                    else:
                                        number_of_frames = (int)(os.path.getsize(os.path.join(clip_directory,clip_))/(width*height+(width*height/2)))

                                elif (bit_depth == 10):
                                    if ENCODER=="vtm":
                                        config_used = "encoder_randomaccess_vtm.cfg"
                                    else :
                                        config_used = "encoder_randomaccess_main_10bit.cfg"
                                    if downscale_commands or scene_cut_clip:
                                        number_of_frames = seq_list[seq_table_index]['number_of_frames']
                                    elif ten_bit_format == 2:
                                        number_of_frames = (int)(((float)(os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/1.25)
                                    else:
                                        number_of_frames = (int)(((os.path.getsize(os.path.join(clip_directory,clip_)))/(width*height+(width*height/2)))/2)

                            output_encoder = ENCODER
                            if passes == 2:
                                output_encoder = ENCODER + "_2p"
                            output_string = os.path.join(bitstream_folder, output_encoder + "_M" + str(enc_mode) + "_" + clip_[:-4] + "_Q" + str(qp_value))

                            command = []
                            second_command = []

                            if ENCODER == "libaom":
                                command.append("(/usr/bin/time --verbose   ./aomenc")
                                if passes in [1,2]:
                                    command.append(encoder_cfg_list[0]['passes_token']+ str(passes))

                                command.append("--verbose ")
                                command.append(encoder_cfg_list[0]['lag_in_frames_token'] + "25")
                                command.append(encoder_cfg_list[0]['auto_alt_ref_token'] + "1")
                                command.append(encoder_cfg_list[0]['qp_mode_token'])
                                if passes == 1 and cqp == 1:
                                    command.append(encoder_cfg_list[0]['use_fixed_qp_offsets_token'] + "1")
                                    command.append(encoder_cfg_list[0]['enable_tpl_model_token'] + "0")
                                    command.append(encoder_cfg_list[0]['min_gf_interval_token'] + "16")
                                    command.append(encoder_cfg_list[0]['max_gf_interval_token'] + "16")
                                    command.append(encoder_cfg_list[0]['gf_min_pyr_height_token'] + "4")
                                    command.append(encoder_cfg_list[0]['gf_max_pyr_height_token'] + "4")
                                    command.append(encoder_cfg_list[0]['aq_mode_token'] + "0")
                                    command.append(encoder_cfg_list[0]['deltaq_token'] + "0")
                                if ".yuv" in clip_:
                                    command.append(
                                        encoder_cfg_list[0]['bitdepth_token'] + str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['inbitdepth_token']+ str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['width_token'] + str(seq_list[seq_table_index]['width']))
                                    command.append(encoder_cfg_list[0]['height_token'] + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(seq_list[seq_table_index]['fps_num']) + '/' + str(seq_list[seq_table_index]['fps_denom']))

                                command.append(encoder_cfg_list[0]['intra_period_token'] + "72")
                                command.append(encoder_cfg_list[0]['intra_period_token_max'] + "72")
                                command.append(encoder_cfg_list[0]['qp_value_token'] + str(str(qp_value)))
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin")
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_)+"  ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "svt":
                                command.append("(/usr/bin/time --verbose   ./SvtAv1EncApp")
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + " " + str(enc_mode))
                                if passes == 2:
                                    command.append(encoder_cfg_list[0]["passes"] + "2")
                                    command.append(encoder_cfg_list[0]["irefresh_type_token"] + "2")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['width_token'] + " " + str(seq_list[seq_table_index]['width']))
                                    command.append(encoder_cfg_list[0]['height_token'] + " " + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]['bitdepth_token'] + " " + str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['fps_num'] + " " + str(seq_list[seq_table_index]['fps_num']))
                                    command.append(encoder_cfg_list[0]['fps_denom'] + " " + str(seq_list[seq_table_index]['fps_denom']))
                                    if not scene_cut_clip == 1:
                                        command.append(encoder_cfg_list[0]['num_frames_token'] + " " + str(number_of_frames))

                                command.append(encoder_cfg_list[0]['qp_value_token'] + " " + str(str(qp_value)))
                                command.append(encoder_cfg_list[0]['intra_period_token'] + " " + "119")

                                if passes == 2 or cqp == 0 :
                                    command.append(encoder_cfg_list[0]["enable_tpl_model_token"] + "1")
                                else :
                                    command.append(encoder_cfg_list[0]['lad_token'] + " " + "0")
                                    command.append(encoder_cfg_list[0]["enable_tpl_model_token"] + "0")

                                command.append(encoder_cfg_list[0]['lp_token'] + " " + "1")
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_))
                                if passes == 1:
                                    command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin ) ")
                                    command.append("> " + output_string + ".txt" + " 2>&1")
                                elif passes == 2:
                                    command.append("--stats " + output_string + ".stat")
                                    command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin ) ")
                                    command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "x264":
                                command.append("(/usr/bin/time --verbose   ./x264")

                                command.append(encoder_cfg_list[0]["enc_mode_token"] + str(enc_mode))

                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]["input_res_token"] + str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]["fps_token"] + str(seq_list[seq_table_index]['fps_num']) + "/" + str(seq_list[seq_table_index]['fps_denom']))
                                    command.append(encoder_cfg_list[0]["bitdepth_token"] + str(seq_list[seq_table_index]['bitdepth']))
                                command.append(encoder_cfg_list[0]["threads_token"] + "1")
                                command.append(encoder_cfg_list[0]["tune_token"] + "psnr")


                                command.append(encoder_cfg_list[0]["stats_token"]+output_string+".stat")
                                command.append(encoder_cfg_list[0]["qp_value_token"] + str(qp_value))

                                command.append(encoder_cfg_list[0]["intra_period_token"] +  "128")
                                command.append(encoder_cfg_list[0]["intra_period_token_min"] + "128")

                                command.append(encoder_cfg_list[0]["no_scenecut_token"])

                                command.append(encoder_cfg_list[0]["bitstream_token"] + output_string + ".bin")
                                command.append(" " + os.path.join(clip_location, clip_) + ") ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "x265":
                                output_string = ENCODER + "_M" + str(enc_mode) + "_" + clip_[:-4] + "_Q" + str(qp_value)
                                output_string_superfast = ENCODER + "_superfast_M8_" + clip_[:-4] + "_Q" + str(qp_value)
                                command.append("(/usr/bin/time --verbose   ./x265")

                                command.append(encoder_cfg_list[0]["enc_mode_token"] + str(enc_mode))

                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]["input_res_token"]+ str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']))
                                    command.append(encoder_cfg_list[0]["fps_token"] + str(seq_list[seq_table_index]['fps_num']) + "/" + str(seq_list[seq_table_index]['fps_denom']))
                                    command.append(encoder_cfg_list[0]["bitdepth_token"]+ str(seq_list[seq_table_index]['bitdepth']))

                                command.append(encoder_cfg_list[0]["tune_token"]+ " psnr")
                                command.append(encoder_cfg_list[0]["stats_token"]+output_string+".stat")

                                command.append(encoder_cfg_list[0]["qp_value_token"] + str(qp_value))
                                command.append(encoder_cfg_list[0]["intra_period_token"] + "128")
                                command.append(encoder_cfg_list[0]["intra_period_token_min"] + "128")

                                command.append(encoder_cfg_list[0]["frame_threads_token"]+ " 1")
                                command.append(encoder_cfg_list[0]["no_scenecut_token"])
                                command.append(encoder_cfg_list[0]["no_wpp_token"])

                                command.append(" " + os.path.join(clip_location, clip_))
                                command.append(encoder_cfg_list[0]["bitstream_token"] + output_string + ".bin) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")

                            elif ENCODER == "vp9":
                                command.append("(/usr/bin/time --verbose   ./vpxenc")
                                command.append("--verbose ")
                                if passes in [1,2]:
                                    command.append(encoder_cfg_list[0]["passes_token"] + str(passes))
                                command.append(encoder_cfg_list[0]["qp_mode_token"])
                                command.append(encoder_cfg_list[0]["lag_in_frames_token"] + "25")
                                command.append(encoder_cfg_list[0]["auto_alt_ref_token"] + "6")
                                command.append(encoder_cfg_list[0]["threads_token"] + "1")

                                if ".yuv" in clip_:
                                    if int(seq_list[seq_table_index]['bitdepth']) == 8:
                                        command.append( " --profile=0 " )
                                    else :
                                        command.append( " --profile=2 " )
                                    command.append(
                                        encoder_cfg_list[0]['bitdepth_token'] + str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['inbitdepth_token']+ str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['width_token'] + str(seq_list[seq_table_index]['width']))
                                    command.append(encoder_cfg_list[0]['height_token'] + str(seq_list[seq_table_index]['height']))
                                    command.append(
                                        encoder_cfg_list[0]['fps_token'] + str(seq_list[seq_table_index]['fps_num']) + '/' + str(
                                            seq_list[seq_table_index]['fps_denom']))
                                command.append(encoder_cfg_list[0]['intra_period_token'] + "128")
                                command.append(encoder_cfg_list[0]['intra_period_token_max'] + "128")
                                command.append(encoder_cfg_list[0]['qp_value_token'] + str(str(qp_value)))
                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin")
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_)+"  ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")


                            elif ENCODER == "ffmpeg":
                                command.append("(/usr/bin/time --verbose ./ffmpeg -y")
                                if ".yuv" in clip_:
                                    command.append(encoder_cfg_list[0]['input_res_token'] + "{}x{}".format(str(seq_list[seq_table_index]['width']), str(seq_list[seq_table_index]['height'])))
                                    if int(seq_list[seq_table_index]['bitdepth']) == 10:
                                        command.append(encoder_cfg_list[0]['bitdepth_token'] + "yuv420p10le")
                                    else:
                                        command.append("-pix_fmt yuv420p")
                                    command.append(encoder_cfg_list[0]['fps_token'] + str(float(seq_list[seq_table_index]['fps_num'])/float(seq_list[seq_table_index]['fps_denom'])))
                                    command.append("-f rawvideo")
                                command.append(encoder_cfg_list[0]['input_token'] + os.path.join(clip_location, clip_))
                                command.append(encoder_cfg_list[0]['qp_value_token'] + str(qp_value))
                                if passes == 2:
                                    print("2 pass commands generation for ffmpeg not supported yet")

                                command.append(encoder_cfg_list[0]['enc_mode_token'] + str(enc_mode))
                                command.append(encoder_cfg_list[0]['intra_period_token'] + "120")
                                command.append(encoder_cfg_list[0]['threads_token'] + "1")
                                command.append(encoder_cfg_list[0]['lad_token'] + "16")
                                command.append(" -c:v libsvtav1")
                                command.append(" -f ivf ")
                                command.append(output_string + ".bin )")
                                command.append("> " + output_string + ".txt" + " 2>&1")
                            else :
                                if ENCODER == "vtm":
                                    encoder_name = "./EncoderAppStatic"
                                else :
                                    encoder_name = "./TAppEncoder"
                                command.append("(/usr/bin/time --verbose   "+encoder_name)
                                command.append(encoder_cfg_list[0]['config_token'] + " " + str(config_used))
                                command.append(encoder_cfg_list[0]['width_token'] + " " + str(seq_list[seq_table_index]['width']))
                                command.append(encoder_cfg_list[0]['height_token'] + " " + str(seq_list[seq_table_index]['height']))
                                if ENCODER == "hm":
                                    command.append(encoder_cfg_list[0]['bitdepth_token']+ str(seq_list[seq_table_index]['bitdepth']))
                                    command.append(encoder_cfg_list[0]['inbitdepth_token']+ str(seq_list[seq_table_index]['bitdepth']))
                                command.append(encoder_cfg_list[0]['qp_value_token'] + " " + str(str(qp_value)))
                                command.append(encoder_cfg_list[0]['fps'] + " " + str(float((seq_list[seq_table_index]['fps_num'])/(seq_list[seq_table_index]['fps_denom']))))
                                if not scene_cut_clip == 1:
                                    command.append(encoder_cfg_list[0]['num_frames_token'] + " " + str(number_of_frames))
                                command.append(encoder_cfg_list[0]['intra_period_token'] + " " + "64")
                                command.append(encoder_cfg_list[0]['input_token'] + " " + os.path.join(clip_location,clip_))
                                command.append(encoder_cfg_list[0]['bitstream_token'] + " " + output_string + ".bin ) ")
                                command.append("> " + output_string + ".txt" + " 2>&1")


                            ffmpeg_string      = "tools/ffmpeg"
                            model_path         = "tools/model/"
                            if psnr_ssim_generation == 1:
                                if ".y4m" in clip_:
                                    txt_file_write = "tools/ffmpeg -r 25  -i " + output_string + ".bin" + " -r 25 -i " + os.path.join(clip_location, clip_) \
                                        + " -lavfi \"ssim=stats_file=" + output_string + ".ssim;[0:v][1:v]psnr=stats_file=" + output_string + ".psnr\""  \
                                        " -f null - > " + output_string + ".log 2>&1"
                                elif ".yuv" in clip_:
                                    if "10bit" in output_string:
                                        pix_fmt = "yuv420p10le"
                                    else:
                                        pix_fmt = "yuv420p"
                                    txt_file_write = ("tools/ffmpeg" \
                                                    + " -r 25 -i " + output_string + ".bin" \
                                                    + " -s " + str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']) \
                                                    + " -f rawvideo -pix_fmt "+ pix_fmt \
                                                    + " -r 25 -i " + os.path.join(clip_location, clip_) \
                                                    + " -lavfi " + "\"" + "ssim=""stats_file=" \
                                                    + output_string + ".ssim" + ";[0:v][1:v]""psnr=""stats_file=" + output_string \
                                                    + ".psnr\" -f null -  > " + output_string + ".log 2>&1")
                                    if ENCODER == "vtm":
                                        vtm_decoder = "./DecoderAppStatic"
                                        vtm_decode_string = (vtm_decoder \
                                                            + " -b " + output_string +".bin"\
                                                            + " -o " + output_string +".yuv")
                                        vtm_metrics_command_to_write = (ffmpeg_string \
                                                                    + " -y -nostdin " \
                                                                    + " -r 25" \
                                                                    + " -f rawvideo" \
                                                                    + " -pix_fmt "+ pix_fmt \
                                                                    + " -s:v " + str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']) \
                                                                    + " -i " + output_string+ ".yuv" \
                                                                    + " -f rawvideo" \
                                                                    + " -r 25 -pix_fmt "+ pix_fmt \
                                                                    + " -s:v " + str(seq_list[seq_table_index]['width']) + "x" + str(seq_list[seq_table_index]['height']) \
                                                                    + " -i " + os.path.join(clip_location, clip_) \
                                                                    + " -lavfi \'[0:v][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                    + "[0:v][1:v]psnr=stats_file=" + output_string + ".psnr\'" \
                                                                    + "-f null - " \
                                                                    + " > " + output_string + ".log 2>&1")
                                        clean_up_yuv_command = "rm " + output_string + ".yuv"

                                        txt_file_write = vtm_decode_string + "&&" + vtm_metrics_command_to_write + "&&" + clean_up_yuv_command

                                ffmpeg_file[ENC_MODES.index(enc_mode)].write(txt_file_write + os.linesep)

                            if vmaf_generation == 1:
                                if "10bit" in output_string:
                                        pix_fmt = "yuv420p10le"
                                else:
                                        pix_fmt = "yuv420p"
                                if disable_enhancement_vmaf ==1:
                                    model_vmaf = "vmaf_v0.6.1neg.pkl"
                                else :
                                    model_vmaf = "vmaf_v0.6.1.pkl"

                                if ".yuv" in clip_:
                                    vmaf_txt_file_write = (ffmpeg_string \
                                                        + " -y -nostdin " \
                                                        + " -r 25" \
                                                        + " -i " + output_string+ ".bin" \
                                                        + " -f rawvideo" \
                                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                                        + " -s:v " + "{}x{}".format(width,height) \
                                                        + " -i " + os.path.join(clip_location, clip_) \
                                                        + " -lavfi \'[0:v][1:v]libvmaf=model_path={}:log_path={}\' -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                        + " > " + output_string + ".vmaf_log 2>&1")
                                elif ".y4m" in clip_:
                                    vmaf_txt_file_write = (ffmpeg_string \
                                                        + " -y -nostdin " \
                                                        + " -i " + output_string+ ".bin" \
                                                        + " -i " + os.path.join(clip_location, clip_) \
                                                        + " -lavfi \'[0:v][1:v]libvmaf=model_path={}:log_path={}\' -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                        + " > " + output_string + ".vmaf_log 2>&1")


                                vmaf_file[ENC_MODES.index(enc_mode)].write(vmaf_txt_file_write + os.linesep)
                            if rescale_bdrate == 1:
                                if "10bit" in output_string:
                                    pix_fmt = "yuv420p10le"
                                else:
                                    pix_fmt = "yuv420p"
                                if disable_enhancement_vmaf ==1:
                                    model_vmaf = "vmaf_v0.6.1neg.pkl"
                                else :
                                    model_vmaf = "vmaf_v0.6.1.pkl"

                                match = re.search(r"_\d+x\d+to\d+x\d+_{}".format(downscale_algo[:4]),clip_)
                                resolution = ""
                                if match is None:
                                    match = re.search(r"_\d+x\d+",clip_)
                                    match_start = match.start()
                                    match_end = match.end()
                                    resolution = clip_[match_start+1:match_end]
                                    ref_res = resolution
                                else:
                                    match_start = match.start()
                                    match_end = match.end()
                                    resolution = clip_[match_start+1:match_end-5].split("to")[1]
                                    ref_res = clip_[match_start+1:match_end-5].split("to")[0]

                                ref_clip_name = clip_[:match_start+1] + ref_res + clip_[match_end:]

                                if ".yuv" in clip_:
                                    if width >= 4096 and height >= 2160:
                                        model_vmaf = "vmaf_4k_v0.6.1.pkl"

                                    metrics_command_to_write = (ffmpeg_string \
                                                                + " -y -nostdin " \
                                                                + " -r 25" \
                                                                + " -i " + output_string+ ".bin" \
                                                                + " -f rawvideo" \
                                                                + " -r 25 -pix_fmt "+ pix_fmt \
                                                                + " -s:v " + ref_res \
                                                                + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                + " -lavfi \'scale2ref=flags={}+accurate_rnd+print_info [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; ".format(downscale_algo) \
                                                                + "[scaled1][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                + "[scaled2][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                + "[scaled3][1:v]libvmaf=model_path={}:log_path={}' -map \"[ref]\" -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                                + " > " + output_string + ".log 2>&1")
                                    if ref_res == resolution:
                                        metrics_command_to_write = (ffmpeg_string \
                                                                    + " -y -nostdin " \
                                                                    + " -r 25" \
                                                                    + " -i " + output_string+ ".bin" \
                                                                    + " -f rawvideo" \
                                                                    + " -r 25 -pix_fmt "+ pix_fmt \
                                                                    + " -s:v " + ref_res \
                                                                    + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                    + " -lavfi \'" \
                                                                    + "[0:v][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                    + "[0:v][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                    + "[0:v][1:v]libvmaf=model_path={}:log_path={}' -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                                    + " > " + output_string + ".log 2>&1")
                                    if ENCODER == "vtm":
                                        vtm_decoder = "./DecoderAppStatic"
                                        vtm_decode_string = (vtm_decoder \
                                                            + " -b " + output_string +".bin"\
                                                            + " -o " + output_string +".yuv")
                                        vtm_metrics_command_to_write = (ffmpeg_string \
                                                                    + " -y -nostdin " \
                                                                    + " -r 25" \
                                                                    + " -f rawvideo" \
                                                                    + " -pix_fmt "+ pix_fmt \
                                                                    + " -s:v " + resolution \
                                                                    + " -i " + output_string+ ".yuv" \
                                                                    + " -f rawvideo" \
                                                                    + " -r 25 -pix_fmt "+ pix_fmt \
                                                                    + " -s:v " + ref_res \
                                                                    + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                    + " -lavfi \'scale2ref=flags={}+accurate_rnd+print_info [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; ".format(downscale_algo) \
                                                                    + "[scaled1][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                    + "[scaled2][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                    + "[scaled3][1:v]libvmaf=model_path={}:log_path={}' -map \"[ref]\" -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                                    + " > " + output_string + ".log 2>&1")
                                        if ref_res == resolution:
                                            vtm_metrics_command_to_write = (ffmpeg_string \
                                                                        + " -y -nostdin " \
                                                                        + " -r 25" \
                                                                        + " -f rawvideo" \
                                                                        + " -pix_fmt "+ pix_fmt \
                                                                        + " -s:v " + resolution \
                                                                        + " -i " + output_string+ ".yuv" \
                                                                        + " -f rawvideo" \
                                                                        + " -r 25 -pix_fmt "+ pix_fmt \
                                                                        + " -s:v " + ref_res \
                                                                        + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                        + " -lavfi \'" \
                                                                        + "[0:v][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                        + "[0:v][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                        + "[0:v][1:v]libvmaf=model_path={}:log_path={}' -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                                        + " > " + output_string + ".log 2>&1")
                                        clean_up_yuv_command = "rm " + output_string + ".yuv"

                                        metrics_command_to_write = vtm_decode_string + "&&" + vtm_metrics_command_to_write + "&&" + clean_up_yuv_command

                                elif ".y4m" in clip_:
                                    if "4096" in ref_res and "2160" in ref_res:
                                        model_vmaf = "vmaf_4k_v0.6.1.pkl"

                                    metrics_command_to_write = (ffmpeg_string \
                                                                + " -y -nostdin " \
                                                                + " -r 25 " \
                                                                + " -i " + output_string+ ".bin" \
                                                                + " -r 25 " \
                                                                + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                + " -lavfi \'scale2ref=flags={}+accurate_rnd+print_info [scaled][ref];[scaled] split=3 [scaled1][scaled2][scaled3]; ".format(downscale_algo) \
                                                                + "[scaled1][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                + "[scaled2][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                + "[scaled3][1:v]libvmaf=model_path={}:log_path={}' -map \"[ref]\" -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                                + " > " + output_string + ".log 2>&1")
                                    if ref_res == resolution:
                                        metrics_command_to_write = (ffmpeg_string \
                                                                    + " -y -nostdin " \
                                                                    + " -r 25 " \
                                                                    + " -i " + output_string+ ".bin" \
                                                                    + " -r 25 " \
                                                                    + " -i " + os.path.join(clip_location, ref_clip_name) \
                                                                    + " -lavfi \'" \
                                                                    + "[0:v][1:v]ssim=stats_file=" + output_string +".ssim;" \
                                                                    + "[0:v][1:v]psnr=stats_file=" + output_string + ".psnr;" \
                                                                    + "[0:v][1:v]libvmaf=model_path={}:log_path={}' -f null -".format(os.path.join(model_path,model_vmaf), output_string + ".vmaf") \
                                                                    + " > " + output_string + ".log 2>&1")

                                rescale_metrics_file[ENC_MODES.index(enc_mode)].write(metrics_command_to_write + os.linesep)

                            command.append("\n")
                            command_ = " ".join(command)
                            fo[ENC_MODES.index(enc_mode)].write(command_)
                            if second_command:
                                second_command.append("\n")
                                second_command_.append(" ".join(second_command))
                                fo[ENC_MODES.index(enc_mode)].write(second_command_)

        fo[ENC_MODES.index(enc_mode)].write(os.linesep)
        fo[ENC_MODES.index(enc_mode)].write(os.linesep)

    for enc_mode in ENC_MODES:
        fo[ENC_MODES.index(enc_mode)].close()
        if psnr_ssim_generation == 1:
            ffmpeg_file[ENC_MODES.index(enc_mode)].close()
        if vmaf_generation ==1 :
            vmaf_file[ENC_MODES.index(enc_mode)].close()
        if rescale_bdrate == 1:
            rescale_metrics_file[ENC_MODES.index(enc_mode)].close()

generate_commands(CLIP_DIRS, clip_location, ENC_MODES, passes, cqp, psnr_ssim_generation, vmaf_generation, disable_enhancement_vmaf,
                      scene_cut_clip, scene_changes_suffix, downscale_commands, rescale_bdrate, target_resolutions,
                      bitstream_folders, ENCODER,seq_list)

#### GENERATE BASH FILE
if cqp == 1:
    encoder_test_identifier = ENCODER +"-" +str(passes)+"p-cqp"
else:
    encoder_test_identifier = ENCODER +"-" +str(passes)+"p"

run_all_paral_file_name = encoder_test_identifier + '-run-all-paral.sh'
run_paral_cpu_file_name = encoder_test_identifier + '-run-paral-cpu.sh'
generate_bash_driver_file(parallel_job_count, ffmpeg_job_count, ENCODER, run_all_paral_file_name, run_paral_cpu_file_name, ENC_MODES, cqp, scene_cut_clip, bitstream_folders, delete_intermediary)
