# Copyright (c) 2019, Alliance for Open Media. All rights reserved
#
# This source code is subject to the terms of the BSD 2 Clause License and
# the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
# was not distributed with this source code in the LICENSE file, you can
# obtain it at www.aomedia.org/license/software. If the Alliance for Open
# Media Patent License 1.0 was not distributed with this source code in the
# PATENTS file, you can obtain it at www.aomedia.org/license/patent.

from __future__ import print_function  # Only needed for Python 2
import os
import string
import re


## encoder template
encoder                     = "svt"
# encoder                     = "aom"
# encoder                     = "vtm"
# encoder                     = "HM"
# encoder                   = "x264"
# encoder                   = "x265"
# encoder                   = "ffmpeg"

## one log file extension present in folder
f_ext                       = ".log"

## only files with prefix will be parsed
prefix                      = ""
log_folder_path             = "bitstreams"

## print output to log
output_log                  = 1
log_name                    = "_results.txt"

## mute console output
## this will be forced ON if output_log == 0 ||  log_name == ""
silent_console              = 1

## Get enc name displayed is based on the parent folder name (..) [if this option is disabled, the file name would be used to extract the enc_name]
get_enc_name_from_folder    = 0

## Force enc name to be the value of enc_name [this option supersedes both options above] -> This now parses delimiter at the end from file name
force_enc_name              = 1
enc_name                    = "svt-av1"

#### This is the 8/1/1 PSNR SSIM script
y_scale                     = 8     ## 8/1/1

#### This is for getting the true encode/decode resolution based on name
downscale_upscale_bdrate    = 1

#### This flag indicates whether or not the vmaf_rc executable was used
vmaf_rc_flag                = 0

#### This flag indicates collecting the bitstream size **INSTEAD** of bitrate
filesize_instead            = 0

#### Convex Hull Calculation
compute_convex_hull        = 1

# metrics to compute the convex hull on
# choose from a mix of psnr, vmaf, and ssim
cvh_metrics = ["psnr","vmaf", "ssim"]

#output file for convex hull points
output_file_suffix         = "_convex_hull_data.txt"
max_ch_pts  = 25  # add lines until this number is reached

def print_log(out_to_log,mode, index_i):

    if silent_console != 0:
        print (out_to_log)
    if output_log == 1 and log_name != "":
        try:
            print (out_to_log,file=open(log_name,mode))
        except:
            if silent_console == 0:
                print (out_to_log)
    elif silent_console == 0:
        print (out_to_log)

out_header =   ("Codec"
             + "\t" + "EncoderName"
             + "\t" + "Resolution"
             + "\t" + "Bit-Depth"
             + "\t" + "InputSequence"
             + "\t" + "QP"
             + "\t" + "Kbps"
             + "\t" + "PSNR(Y)"
             + "\t" + "PSNR(U)"
             + "\t" + "PSNR(V)"
             + "\t" + "PSNR(ALL)"
             + "\t" + "SSIM(Y)"
             + "\t" + "SSIM(U)"
             + "\t" + "SSIM(V)"
             + "\t" + "SSIM(ALL)"
             + "\t" + "VMAF"
             + "\t" + "enc_time(ms)"
             + "\t" + "wall_time(ms)"
             + "\t" + "max_memory(kb)"
             + "\t" + "sys_time(ms)")

if filesize_instead == 1:
    out_header =   ("Codec"
                 + "\t" + "EncoderName"
                 + "\t" + "Resolution"
                 + "\t" + "Bit-Depth"
                 + "\t" + "InputSequence"
                 + "\t" + "QP"
                 + "\t" + "FileSize (bytes)"
                 + "\t" + "PSNR(Y)"
                 + "\t" + "PSNR(U)"
                 + "\t" + "PSNR(V)"
                 + "\t" + "PSNR(ALL)"
                 + "\t" + "SSIM(Y)"
                 + "\t" + "SSIM(U)"
                 + "\t" + "SSIM(V)"
                 + "\t" + "SSIM(ALL)"
                 + "\t" + "VMAF"
                 + "\t" + "enc_time(ms)"
                 + "\t" + "wall_time(ms)"
                 + "\t" + "max_memory(kb)"
                 + "\t" + "sys_time(ms)")


if encoder == 'x264' or encoder == 'x265':
    encoder_mode = -1
else:
    encoder_mode = 99

files_dir_list = []

bitstreams_files = os.listdir(log_folder_path)
bitstreams_files.sort()
bitstreams_files = [ os.path.join(log_folder_path, i) for i in bitstreams_files]
files_dir_list.append(bitstreams_files)

for index_i in range(len(files_dir_list)):
    files = files_dir_list[index_i]
    print_log(out_header,'w', index_i);
    for file_ in files:

        if f_ext in file_ and prefix in file_:
            file_name_with_ext = os.path.basename(file_)
            dir_name = os.path.dirname(file_)
            file_name = os.path.splitext(file_name_with_ext)[0]
        #  read psnr ssim output
            try:
                if ".log" in file_:
                    psnr_ssim_log = open(os.path.join(dir_name, file_name_with_ext),"r")
                else:
                    psnr_ssim_log = open(file_name + ".log","r")
                lines = psnr_ssim_log.readlines()
                ssim_line = ""
                psnr_line = ""
                res_line = ""
                for line in lines:
                    if "Parsed_ssim" in line:
                        ssim_line = line
                    if "Parsed_psnr" in line:
                        psnr_line = line
                    if "yuv420p" in line:
                        res_line = line
            except:
                ssim_line = ""
                psnr_line = ""
                res_line = ""

        #  read vmaf output
            try:
                with open(os.path.join(dir_name, file_name+'.vmaf'),"r") as vmaf_log:
                    lines = vmaf_log.readlines()
                    vmaf_line = ""
                    for line in lines:
                        if not vmaf_rc_flag and 'aggregateVMAF' in line:
                            vmaf_line = line
                        if vmaf_rc_flag and "pkl:" in line:
                            vmaf_line = line

            except:
                vmaf_line = ""
                pass

            try:

                bitstream_file_name = os.path.join(dir_name, file_name + '.bin')
                stat_struct = os.stat(bitstream_file_name)
                bitstreams_size = stat_struct.st_size
                # print(bitstream_file_name + " with size " + str(bitstreams_size))
            except:
                bitstreams_size = "err_filesize"
                pass


        #  read bitrate / speed output
            try:
                encode_log = open(os.path.join(dir_name, file_name + ".txt"),"r")
                lines = encode_log.readlines()
                bitrate_line = ""
                enc_time_line = ""
                sys_time_line = ""
                svt_1st_pass_time_line =""
                memory_line = ""
                vtm_flag = 0
                HM_flag = 0
                Num_passes = 0
                for raw_line in lines:
                    line = str(raw_line).lower()
                    if "vtm" in encoder:
                        if "bitrate" in line:
                            vtm_flag = 1
                        elif vtm_flag == 1:
                            bitrate_line = line
                            vtm_flag = 0
                        if "user" in line:
                            enc_time_line = line
                    if "HM" in encoder:
                        if "summary" in line:
                            HM_flag = 1
                        elif HM_flag == 1:
                            HM_flag = 2
                        elif HM_flag == 2:
                            bitrate_line = line
                            HM_flag = 0
                        if "user" in line:
                            enc_time_line = line
                    if "bps" in line and "HM" not in encoder and "aom" not in encoder and "command being timed" not in line:
                        bitrate_line = line
                    elif "b/s" in line :
                        bitrate_line = line
                    if "ffmpeg" in encoder and "bitrate=" in line:
                        bitrate_line = line
                    if "user" in line and "svt" in encoder:
                        enc_time_line = line
                    if "user" in line and "x264" in encoder:
                        enc_time_line = line
                    if "user" in line and "x265" in encoder:
                        enc_time_line = line
                    if "user" in line and "aom" in encoder:
                        enc_time_line = line
                    if "user" in line and "ffmpeg" in encoder:
                        enc_time_line = line
                    if "system" in line and "time" in line:
                        sys_time_line = line
                    if "maximum" in line :
                        memory_line = line
                    if "encoder finished" in line:
                        Num_passes = Num_passes + 1
                if Num_passes == 2 and "svt" in encoder:
                    for raw_line in lines:
                        line = str(raw_line).lower()
                        if "user time" in line:
                            svt_1st_pass_time_line = line
                            break
            except:
                bitrate_line = ""
                enc_time_line = ""
                sys_time_line = ""
                memory_line = ""
                pass

        #  get output data
        #  ssim data
            if ssim_line != "":
                ssim_pieces = ssim_line.split()
                y_ssim = ssim_pieces[5][1:-1]
                u_ssim = ssim_pieces[7][1:-1]
                v_ssim = ssim_pieces[9][1:-1]
                yuv_ssim = (y_scale * float(y_ssim) + float(u_ssim) + float(v_ssim)) / (y_scale + 1 + 1)
            else:
                y_ssim = "err_ssim_y"
                u_ssim = "err_ssim_u"
                v_ssim = "err_ssim_v"
                yuv_ssim = "err_ssim_yuv"

        #  psnr data
            if psnr_line != "":
                psnr_pieces = psnr_line.split()
                y_psnr = psnr_pieces[4][2:]
                u_psnr = psnr_pieces[5][2:]
                v_psnr = psnr_pieces[6][2:]
                yuv_psnr = (y_scale * float(y_psnr) + float(u_psnr) + float(v_psnr)) / (y_scale + 1 + 1)
            else:
                y_psnr = "err_psnr_y"
                u_psnr = "err_psnr_u"
                v_psnr = "err_psnr_v"
                yuv_psnr = "err_psnr_yuv"

        #  vmaf data
            if vmaf_line != "" and not vmaf_rc_flag:
                vmaf_pieces = vmaf_line.split("aggregateVMAF=\"")
                y_vmaf = vmaf_pieces[1].split()[0][:-1]
            elif vmaf_line != "" and vmaf_rc_flag:
                vmaf_pieces = vmaf_line.split(":")
                y_vmaf = vmaf_pieces[1].split()[0][:-1]
            else:
                y_vmaf = "err_vmaf"

        #  bitrate data
            if bitrate_line != "":
                bitrate_pieces = bitrate_line.split()
                if "svt" in encoder:
                    bitrate = bitrate_pieces[4]
                elif "aom" in encoder:
                    if "bps" in bitrate_line:
                    #    print (bitrate_pieces[-4])
                        bitrate = float (bitrate_pieces[-4]) / 1000
                    else:
                        bitrate = float (bitrate_pieces[-5][:-3]) / 1000
                elif "vtm" in encoder:
                    bitrate = bitrate_pieces[2]
                elif "HM" in encoder:
                    bitrate = bitrate_pieces[2]
                elif "x264" in encoder:
                    bitrate = bitrate_pieces[5]
                elif "x265" in encoder:
                    bitrate = bitrate_pieces[7]
                else:
                    bitrate = "err_encoder"
            else:
                bitrate = "err_bitrate"
            if filesize_instead == 1:
                bitrate = bitstreams_size
        #  memory data
            try:
                if memory_line != "":
                    max_memory = "err_memory"
                    memory_pieces = memory_line.split()
                    mem_cap = memory_pieces[5]
                    max_memory = mem_cap
                else:
                    max_memory = "err_memory"
            except:
                max_memory = "err_memory"

        #  resolution data
            if res_line != "":
                res_pieces = res_line.split()
                res = res_pieces[5]
                res = res.split(",")[0]
                bd = res_pieces[4][:-1]
                if "10le" in bd:
                    bd = "10bit"
                else:
                    bd = "8bit"
                if downscale_upscale_bdrate:
                    resolution_pattern  =  r"(\d\d\d?\d?x\d\d\d?\d?)"
                    last_match = re.findall(resolution_pattern,file_)[-1]
                    res = last_match

            else:
                res = "err_res"
                bd = "err_bit-depth"

        #  encode time data
            if enc_time_line != "":
                enc_time_line = enc_time_line.replace('user', ' ')
                enc_time_pieces = enc_time_line.split()
                enc_time = enc_time_pieces[0]
                if "time" not in enc_time_line:
                    mins = float(enc_time_pieces[0][:enc_time_pieces[0].rfind('m')])
                    secs = float(enc_time_pieces[0][enc_time_pieces[0].rfind('m')+1:-1])

                    enc_time = str(((mins*60+secs)))
                    enc_time = str(int(float(enc_time)*1000))
                else:
                    if Num_passes == 2 and "svt" in encoder:
                        svt_1st_pass_time_line = svt_1st_pass_time_line.replace('user', ' ')
                        enc_time_pieces_1st = svt_1st_pass_time_line.split()
                        enc_time = float(float(enc_time_pieces[2] )+ float(enc_time_pieces_1st[2]))
                        enc_time = str(int(float(enc_time)*1000))
                    else:
                        enc_time = enc_time_pieces[2]
                        enc_time = str(int(float(enc_time)*1000))
            else:
                enc_time = "err_enc_time"

            #  system time data
            if sys_time_line != "":
                sys_time_line = sys_time_line.replace('system', ' ')
                sys_time_pieces = sys_time_line.split()
                sys_time = sys_time_pieces[0]
                if "time" not in sys_time_line:
                    print(sys_time_line)
                    mins = float(sys_time_pieces[0][:sys_time_pieces[0].rfind('m')])
                    secs = float(sys_time_pieces[0][sys_time_pieces[0].rfind('m')+1:-1])

                    sys_time = str(((mins*60+secs)))
                    sys_time = str(int(float(sys_time)*1000))
                else:
                    if Num_passes == 2 and "svt" in encoder:
                        svt_1st_pass_time_line = svt_1st_pass_time_line.replace('system', ' ')
                        sys_time_pieces_1st = svt_1st_pass_time_line.split()
                        sys_time = float(float(sys_time_pieces[2] )+ float(sys_time_pieces_1st[2]))
                        sys_time = str(int(float(sys_time)*1000))
                    else:
                        sys_time = sys_time_pieces[2]
                        sys_time = str(int(float(sys_time)*1000))
            else:
                sys_time = "err_sys_time"

        #  qp data
            if "Q" in file_name:
                qp = file_name[-2:]
            else:
                qp = "err_qp"

        #  get encoder_name
            # delim_pos = file_name.rfind(delimiter)
            try:
                delim_pos = re.search(r"_M\d",file_name).start()
            except:
                delim_pos = 0
                pass
            delimiter = file_name[delim_pos:delim_pos+3]
            seq_name = file_name[delim_pos+4:-4]
            if downscale_upscale_bdrate == 1:
                try:
                    to_delim = re.search(r"\d+x\d+to\d+x\d+",seq_name)
                    res_length = len(seq_name[to_delim.start():to_delim.end()].split("to")[1])
                    seq_name = seq_name[:to_delim.start()] + seq_name[to_delim.end()-res_length:]
                except:
                    pass
            encoder_name = file_name[:delim_pos+3]

            wall_time_line = ""
            wall_time = ""

            enc_mode = delimiter.split('M')[1]
            time_enc_log_file_name = "time_enc_" + str(enc_mode) + ".log"
            if os.path.exists(time_enc_log_file_name):
                if int(enc_mode) < int(encoder_mode) and encoder != 'x264' and encoder != 'x265':
                    encoder_mode = str(enc_mode)
                elif int(enc_mode) > int(encoder_mode) and (encoder == 'x264' or encoder == 'x265'):
                    encoder_mode = str(enc_mode)
            else:
                enc_mode = str(encoder_mode)
                time_enc_log_file_name = "time_enc_" + str(enc_mode) + ".log"

            try:
                total_time_log = open(time_enc_log_file_name, 'r')

                log_lines = total_time_log.read().splitlines()
                for line in log_lines:
                    ## Check if using bash time instead of gnu time
                    if 'elapsed' in line.lower():
                        wall_time_line = line
                    elif 'real' in line.lower():
                        wall_time_line = line
                # print('\nwalltime : {}\n'.format(wall_time_line))
                if wall_time_line != "":
                    if "real" in wall_time_line :
                        wall_time_line = wall_time_line.replace('real', ' ')
                        wall_time_pieces = wall_time_line.split()
                        wall_time = wall_time_pieces[0]
                        if "CPU" not in wall_time_line:
                            mins = float(wall_time_pieces[0][:wall_time_pieces[0].rfind('m')])
                            secs = float(wall_time_pieces[0][wall_time_pieces[0].rfind('m')+1:-1])

                            wall_time = str(((mins*60+secs)))
                            wall_time = str(int(float(wall_time)*1000))
                    elif "Elapsed" in wall_time_line :
                        wall_time_pieces = wall_time_line.split()
                        wall_time = wall_time_pieces[7]

                        # NOTE: Will break if gnu time starts to use days past a certain threshold b/c Hours only pops up past 2 hours
                        mins = wall_time[:wall_time.rfind(':')]
                        hours = 0
                        if ":" in mins:
                            hours =  float(mins[:mins.rfind(':')])
                            mins = float(mins[(mins.rfind(':')+1):])
                        else:
                            mins = float(mins)
                        secs = float(wall_time[(wall_time.rfind(':')+1):])
                        wall_time = float(((hours*3600+mins*60+secs)))
                        wall_time = str(int(float(wall_time)*1000))

                else:
                        wall_time = "err_wall_time"
            except:
                wall_time = "err_wall_time"

            # print(wall_time)

            if get_enc_name_from_folder:
                path = os.path.abspath("..")
                parent_folder = os.path.basename(path)
                encoder_name = parent_folder+file_name[delim_pos:delim_pos+3]

            if force_enc_name:
                encoder_name = enc_name + file_name[delim_pos:delim_pos] + delimiter

            output =   (   str(encoder)
                        +"\t" + str(encoder_name)
                        +"\t" + str(res)
                        +"\t" + str(bd)
                        +"\t" + str(seq_name)
                        +"\t" + str(qp)
                        +"\t" + str(bitrate)
                        +"\t" + str(y_psnr)
                        +"\t" + str(u_psnr)
                        +"\t" + str(v_psnr)
                        +"\t" + str(yuv_psnr)
                        +"\t" + str(y_ssim)
                        +"\t" + str(u_ssim)
                        +"\t" + str(v_ssim)
                        +"\t" + str(yuv_ssim)
                        +"\t" + str(y_vmaf)
                        +"\t" + str(enc_time)
                        +"\t" + str(wall_time)
                        +"\t" + str(max_memory)
                        +"\t" + str(sys_time)
                           )

            print_log (output,'a', index_i);


if compute_convex_hull == 1:
    for cvh_metric in cvh_metrics:
        f_in        = open(log_name,'r');
        lines       = f_in.readlines();
        all_lines   = [[]]
        clip_lines  = []
        active_clip = ""
        temp_clip   = ""
        header = ""
        output_file = cvh_metric + output_file_suffix

        for l in lines:
            if "EncoderName" in l:
                header = l
                continue

            if active_clip != "":
                if active_clip in l and encoder_name in l:
                    clip_lines.append(l)
                else:
                    # new clip
                    #print (clip_lines)
                    all_lines.append(clip_lines)
                    clip_lines = []

            if clip_lines == []:

                # reset
                active_clip = ""
                clip_name = ""
                encoder_name = ""

                # new clip
                clip_lines.append(l)
                pieces = l.split("\t")

                # check length of line
                if len(pieces) != 20:
                    print ("File format does not seem to be correct")
                    continue

                # extract clip name
                clip_name = pieces[4]
                encoder_name = pieces[1]

                # extract root clip name
                match = re.search("_\d+x\d+", clip_name)
                if match is not None:
                    active_clip = clip_name[:match.start()]
                else:
                    nm_p = clip_name.split("_")
                    if len(nm_p) < 2:
                        active_clip = clip_name

                    active_clip = nm_p[0]+"_"+nm_p[1]


        # add last clip
        all_lines.append(clip_lines)


        if header == "":
            header = "Codec\tEncoderName\tResolution\tBit-Depth\tInputSequence\tQP\tKbps\tPSNR(Y)\tPSNR(U)\tPSNR(V)\tPSNR(ALL)\tSSIM(Y)\tSSIM(U)\tSSIM(V)\tSSIM(ALL)\tVMAF\tenc_time(ms)\twall_time(ms)\tmax_memory(kb)"


        print (header,file=open( output_file,'w'))

        for i in range(len(all_lines)):

            conv_h_exec  = "./tools/convex_hull_exe "
            rates       = " "
            metric      = " "
            new_lines   = []
            new_lines_vmaf = []
            if all_lines[i] == []:
                continue
            for line in all_lines[i]:

                line_s = line.split("\t")
                if len(line_s) != 20:
                    print ("File format does not seem to be correct")
                    continue

                rates   += (line_s[6] + "  ")

                if cvh_metric == "psnr":
                    metric += (line_s[7] + "  ")
                elif cvh_metric == "vmaf":
                    metric += (line_s[15] + "  ")
                else:
                    metric += (line_s[11] + "  ")

            conv_h_cli = conv_h_exec + str(len(all_lines[i])) + rates + metric + " > conv_h_log.txt"

            # run the convex hull commandline
            os.system(conv_h_cli)
            # get the convex hull indices
            index_range = list(range(len(all_lines[i])))

            with open('conv_h_log.txt', 'r') as log:
                lines = log.readlines()
                for line in lines:
                    if "i=" in line:
                        line_s = line.split(",")
                        index_w = line_s[0]
                        index = int(index_w[2:])

                        # prep line to be added
                        tmp_line = all_lines[i][index]
                        index_range.remove(index)
                        clip_name = tmp_line.split("\t")[4]
                        match = re.search("_\d+x\d+", clip_name)
                        if match is not None:
                            sim_clip_name = clip_name[:match.start()]
                        else:
                            sim_clip_name = clip_name.split("_")[0]+"_"+clip_name.split("_")[1]

                        # rename clip to unify the clip name as much as possible
                        rep_line = tmp_line.replace(clip_name,sim_clip_name)
                        rep_line_s = rep_line.split("\t")

                        metric_part = rep_line_s[7]+"\t"+" \t"*8
                        if cvh_metric == "vmaf":
                            metric_part = " \t"*8 + rep_line_s[15]+"\t"
                        elif cvh_metric == "ssim":
                            metric_part = " \t"*4 + rep_line_s[11]+"\t" + " \t"*4

                        fn_line     = (   rep_line_s[0]+"\t"
                                        + rep_line_s[1]+"_"+cvh_metric+"\t"
                                        + rep_line_s[2]+"\t"
                                        + rep_line_s[3]+"\t"
                                        + rep_line_s[4]+"\t"
                                        + rep_line_s[5]+"\t"
                                        + rep_line_s[6]+"\t"
                                        + metric_part
                                        + rep_line_s[16]+"\t"
                                        + rep_line_s[17]+"\t"
                                        + rep_line_s[18]+"\t"
                                        + rep_line_s[19])

                        new_lines.append(fn_line)
            # output the results
            line_count = 0
            for line in reversed(new_lines):
                line_count = line_count + 1
                if line[-1] == "\n":
                    print (line[:-1],file=open( output_file,'a'))
                else:
                    print (line,file=open( output_file,'a'))


            line = new_lines[0]
            if line_count > max_ch_pts:
                print ("\n\n\n\n\n MAX line count reached or exceeded " + str(line_count)+ "\n\n\n\n")
            else:
                max_qp = 64
                for j in index_range:
                    if (line_count<max_ch_pts):
                        filler_pieces = all_lines[i][j].split("\t")
                        filler_line = (   filler_pieces[0]+"\t"
                                        + filler_pieces[1]+"_"+cvh_metric+"\t"
                                        + filler_pieces[2]+"\t"
                                        + filler_pieces[3]+"\t"
                                        + line.split("\t")[4]+"\t"
                                        + str(max_qp)
                                        + " \t"*11
                                        + filler_pieces[16]+"\t"
                                        + filler_pieces[17]+"\t"
                                        + filler_pieces[18]+"\t"
                                        + filler_pieces[19].rstrip())

                        print (filler_line,file=open( output_file,'a'))
                        line_count = line_count + 1
                        max_qp = max_qp + 1
                    else:
                        break
                while(line_count<max_ch_pts):
                    filler_pieces = line.split("\t")
                    filler_line = (   filler_pieces[0]+"\t"
                                    + filler_pieces[1]+"_"+cvh_metric+"\t"
                                    + filler_pieces[2]+"\t"
                                    + filler_pieces[3]+"\t"
                                    + filler_pieces[4]+"\t"
                                    + str(max_qp))

                    print (filler_line,file=open( output_file,'a'))
                    line_count = line_count + 1
                    max_qp = max_qp + 1
