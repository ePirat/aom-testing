License
===
Copyright (c) 2020, Intel Corporation. All rights reserved

This source code is subject to the terms of the BSD 2 Clause License and
the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
was not distributed with this source code in the LICENSE file, you can
obtain it at www.aomedia.org/license/software. If the Alliance for Open
Media Patent License 1.0 was not distributed with this source code in the
PATENTS file, you can obtain it at www.aomedia.org/license/patent.

Requirements
===
Required tools:
- statically built ffmpeg version newer than revision a37109d555 prebuilt linux builds here:
  - https://johnvansickle.com/ffmpeg/
- vmaf models
  - script supports version v0.6.1 found here https://github.com/Netflix/vmaf/tree/master/model
- python 2.7

Documentation
===
The generate_commands_all.py script generates newline separated command lines for encoding a
production-like VOD pipeline which includes 4 stages -- Scene Change Cutting, Downscaling,
Encoding, and Metrics Computation. It also generates a bash script to run these commands.

These are the details of the 4 stages

1. Stage 1: Scene Change Cutting (scene_cut_clip)
This stage generates the commands that given a full length clip and
a text file with the same name containing a new line separated list of
1-index scene starting frames will cut the long clip into its
constituent scenes. Set scene_changes_suffix to match the
nomenclature of the text files

2. Stage 2: Downscaling (downscale_commands)
This stage generates the commands that given a set of target resolutions,
will downscale the clips to those target resolutions using the lanczos scaler

3. Stage 3: Encoding
This stage generates the encoding commands for the given encoder
for either clips generated from the previous stages or if those are
disabled, only the clips found in CLIP_DIRS

4. Stage 4: Metric computation (rescale_bdrate)
This stage generates the commands that resizes the bitstreams back up
to the same resolution as the reference clip and then compute
psnr, ssim and vmaf via ffmpeg

Each non-encoding stage can be individually turned off via their respective flags

Note that when generating commands to encode YUV files, you will have to add an entry for each
clip into the yuv_library.py file using the following format:

```python
seq_list.append({'tbr' : 7000 , 'qp' : 32 , 'width' : 1920 , 'height' : 1080, 'fps_num' : 30000 , 'fps' : 29.97	, 'fps_denom' : 1001	, 'intra' : 119 	, 'bitdepth' : 8   , 'unpacked' : 0  , 'name':'aspen_1080p_60f'})
```

the main fields to populate are the 'width', 'height', 'fps_num', 'fps', 'fps_denom', 'bitdepth', 'unpacked' and most importantly 'name' must match the filename exactly
width and height are the dimensions of the YUV respectively.
fps_num and fps_denom are the fps numerator and denominator
bitdepth is the bit depth.

After encoding, data can be collected by using the collect_raw_bdr_speed_data.py script.
This script collects the data in a tabulated format that is easily to parse by either another
script or by pasting into a spreadsheet. It also optionally computes the convex hull
from the rate distortion curves of each resolution for a given sequence
